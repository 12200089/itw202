import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const MyF = () => {
    return(
        <View style={sty.container}>
            <View style={sty.b1}></View>
            <View style={sty.b2}></View>
        </View>
    )
}
export default MyF;
const sty = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        marginBottom: 100
    },
    b1: {
        height: 100,
        width: 100,
        backgroundColor: 'red'
    },
    b2: {
        height: 100,
        width: 100,
        backgroundColor: 'blue'
    }
})
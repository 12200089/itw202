import { StyleSheet,Button ,Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

function screen1({navigation}){
  // console.log(navigation)
  return(
    <View style={styles.container}>
      <Text>SCREEN 1</Text>
      <Button title='go to the second screen' 
      onPress={()=>{
        navigation.navigate("S2")
      }}/>
      {/* <Button title='go to third screen'
        onPress={()=>{
          navigation.reset({
            index: 0,
            routes: [{name: 'S3'}]
          })
        }}
      /> */}
    </View>
  )
}
function screen2({navigation}){
  return(
    <View style={styles.container}>
      <Text>SCREEN 2</Text>
      <Button title='go to third screen'
        onPress={()=>{
          navigation.replace('S3'); 
        }}
      />
    </View>
  )
}
function screen3({navigation}){
  return(
    <View style={styles.container}>
      <Text>SCREEN 3</Text>
      <Button title='go back'
        onPress={()=>{
          navigation.goBack(); 
        }}
      />
    </View>
  )
}
export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
      initialRouteName='S1' 
      screenOptions={{headerShown: false}}>  
        <Stack.Screen name='S1' component={screen1}/>
        <Stack.Screen name='S2' component={screen2}/>
        <Stack.Screen name='S3' component={screen3}/>
      </Stack.Navigator>
    </NavigationContainer>
    
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
})
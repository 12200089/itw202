import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import React from 'react';

export default class App extends React.Component {
  render = () =>  (
    <View style={styles.container}>
      <Text>Practical 2</Text>
      <Text>Stateless and Stateless components</Text>
      <Text style={styles.text}>
          you are ready to start journey.
      </Text>

    </View>
  );
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    padding: '10%',
    justifyContent: 'center',
  },
  text: {
    marginTop: '5%',
  }
});

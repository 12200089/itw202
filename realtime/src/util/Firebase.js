import { firebase } from '@firebase/app'

const CONFIG = {
  apiKey: "AIzaSyDDXA8JHLFn4Cu-ZBWrQwpzZprEM4P_oF8",
  authDomain: "my-app-c40ad.firebaseapp.com",
  projectId: "my-app-c40ad",
  storageBucket: "my-app-c40ad.appspot.com",
  messagingSenderId: "339755175933",
  appId: "1:339755175933:web:3eb1758255a35f3da09394"
}
firebase.initializeApp(CONFIG)

export default firebase;

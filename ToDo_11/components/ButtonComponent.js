import React, {useState} from 'react';
import {View,Text,StyleSheet,Button} from 'react-native';

const ButtonComponent = () => {
    const [count, setCount] = useState(1);
    const [text, setText] = useState("The Button isn't pressed yet");
    const [disable, setDisable] = useState(false)
    return (
        <View style={styles.container}>
            <Text style={styles.txt}>{text}</Text>
            <Button 
                title="Press Me"
                onPress={()=> {
                    if(count<4){
                        setText("The button was pressed" + count +"times!")
                        setCount(count + 1)
                        if(count==3){
                            setDisable(true);
                        }
                    }
                }}
                disabled={disable}
                />
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: 350,
        justifyContent: 'center',
        
    },
    txt: {
        alignContent: 'center',
        margin: 10,
    }
})
export default ButtonComponent;


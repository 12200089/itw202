import { StyleSheet, Text, View } from 'react-native';
import ButtonComponent from './components/ButtonComponent.js';

export default function App() {
  return (
    <View style={styles.container}>
      <ButtonComponent/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});


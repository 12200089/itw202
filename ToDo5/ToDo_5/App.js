import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import MyComponent from './components/myComponents';

export default function App() {
  return (
    <View style={styles.container}>
      <MyComponent></MyComponent>
      <Text></Text>
      {/* <Text>Getting started with react native!</Text>
      <Text>My name is Sonam Wangmo</Text> */}
      <StatusBar style="auto" />
    </View>
  );
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

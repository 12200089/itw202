import React, {useState} from 'react';
import { Text, View, StyleSheet, TextInput} from 'react-native';

const MyF = () => {
    const [text, setText] = useState('________');
    return (
        <View style={sty.container}>
            <Text style={{fontSize: 20, paddingBottom: 15, fontWeight: 'bold'}}>What is your name?</Text>
            <Text style={{fontSize: 20, paddingBottom: 15}}>Hi {text} from GCIT</Text>
            <TextInput  
            style={sty.txtI}
            placeholder='Enter Text' 
            multiline={false}
            secureTextEntry={true}
            onChangeText={text=>setText(text)}  
            />
        </View>
    )
}
export {MyF};
const sty = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start',
        marginTop: 100,
        padding: 30
    },
    txtI: {
        borderWidth: 2,
        fontSize: 20
    }
})
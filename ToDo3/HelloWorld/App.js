import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import {add, multiply} from './component/nameExport';
import division from './component/defaultExport';
import React from 'react';

export default function App() {
  return (
    <View style={styles.container}>
      <Text>Open up App.js to start working on your app!</Text>
      <Text>Result of addition: {add(5,6)}</Text>
      <Text>Result of multiplication: {multiply(5,8)}</Text>
      <Text>Result of division: {division(10,2)}</Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
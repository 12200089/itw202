import React from "react";
// Named export
// my_math_func1.js
function add(x,y){
    return x+y;
}
function multiply(x,y){
    return x*y; 
}
export {add, multiply};
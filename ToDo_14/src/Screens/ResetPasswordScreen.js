import React, {useState} from 'react'
import {Text, TouchableOpacity, View, StyleSheet} from 'react-native'
import Background from '../components/Background'
import Button from '../components/Button'
import Header from '../components/Header'
import Paragraph from '../components/Paragraph'
import Logo from '../components/Logo'
import TextInput from '../components/TextInputs'
import { emailValidator } from '../core/helpers/emailValidator'
import { passwordValidator } from '../core/helpers/passwordValidator'
import BackButton from '../components/BackButton'
import { theme } from '../core/theme'
import { signInWithEmailAndPassword } from '../api/auth-api'
import { ResetPassword } from '../api/auth-api'


export default function ResetPasswordScreen({navigation}){
    const [email, setEmail] = useState({value: "", error: ""})
    const [loading, setLoading] = useState();

    const onSubmitPressed = async()=>{
        const emailError = emailValidator(email.value);
        if (emailError ){
            setEmail({...email, error: emailError})
        }
        setLoading(true)
        const response = await ResetPassword(email.value)
        if(response.error){
            alert(response.error)
        }
        else{
            navigation.replace('LoginScreen')
            alert("Reset password has been submited successfully in your email")
        }
        setLoading(false)
    }
    return (
        <Background>
            <BackButton goBack={navigation.goBack}/>
            <Logo/>
            <Header>Restore Password</Header>
            <TextInput 
                label='Email'
                value={email.value}
                error={email.error}
                errorText={email.error}
                onChangeText={(text)=> setEmail({ value: text, error: ""})}
                description='You will receive email with password reset link.'
            />
            <Button mode='contained' onPress={onSubmitPressed}>Send Instructions</Button>        
        </Background>
    )
}
import React from "react";
import { View, StyleSheet, Text } from "react-native";

const MyComponent = () =>{
    return(
        <View style = {styles.contain}>
            <View style = {styles.square1}><Text style = {styles.square1}>1</Text></View>
            <View style = {styles.square2}><Text style = {styles.square2}>2</Text></View>
            <View style = {styles.square3}><Text style = {styles.square3}>3</Text></View>

        </View>

    );
}
const styles=StyleSheet.create({
    contain:{
        flexDirection:'row',
        marginBottom: 400,
        marginRight: 35,
    },
    square1:{
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'red',
        width: 50,
        height: 200,  
        textAlign:"center", 
        textAlignVertical: "center"       
    },
    square2:{
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'blue',
        width: 150,
        height: 200,   
        textAlign:"center", 
        textAlignVertical: "center"         
    },
    square3:{
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'green',
        width: 10,
        height: 200, 
        textAlign:"center", 
        textAlignVertical: "center"           
    }
});
export default MyComponent;
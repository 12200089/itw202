import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import styles from './styles.js';

// export default function App() {                                   //inline styles
//   return (
//     <View style={{ flex: 1, justifyContent: 'center',
//       alignItems: 'center', backgroundColor: "#F%FCFF" 
//     }}>
//       <Text style= {{ fonesize: 20, textAlign:
//        'center', margin: 10}}>
//         Welcome to React Native!
//        </Text>
//     </View>
//   );
// }

export default function App() {                                     
  return (
    <View style = {styles.container}>
      <Text style = {styles.welcome}>
        Welcome to React Native!
       </Text>
    </View>
  );
}

// const styles = StyleSheet.create({             //StyleSheet within same file
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
//   welcome: {
//     fontSize: 20,
//     textAlign: 'center',
//     margin: 10
//   }
// });

// export default styles;


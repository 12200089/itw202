import {StyleSheet} from 'react-native'          //StyleSheet as an imported module
const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    welcome: {
      fontSize: 20,
      textAlign: 'center',
      margin: 10
    }
  });
  
  export default styles;
import React from "react";
import {View, StyleSheet, Text} from "react-native";

const MyComponent = () => {
    const greeting = "Ten Gyel";
    const greeting1 = <Text>Sonam Gyelden</Text>
    return (
        <View>
            <Text style ={styles.textStyle}> This is a Demo of JSX</Text>
            <Text>Hi There!!! {greeting}</Text>
            {greeting1}
        </View>
    )
};

const styles = StyleSheet.create({
    textStyle: {
        fontSize: 24
    }
})



export default MyComponent;

import { Alert, Dimensions, KeyboardAvoidingView, ScrollView, StyleSheet, Text, TextInput, useWindowDimensions, View } from 'react-native'
import React,{useState} from 'react'
import PrimaryButton from '../components/ui/PrimaryButton'
import Colors from '../constants/colors';
import Title from '../components/ui/Title';
import Card from '../components/ui/Card';
import InstructionText from "../components/ui/InstructionText"

function StartGameScreen({onPickNumber}) {
  const [enteredNumber, setEnteredNumber]=useState("");

  const {width, height}= useWindowDimensions();
  function numberInputHandler(enteredText){
    setEnteredNumber(enteredText);
  }
  function resetInputHandler(){
    setEnteredNumber('')
  }
  function confirmInputHandler(){
    const choosenNumber=parseInt(enteredNumber)

    if(isNaN(choosenNumber) || choosenNumber <=0 || choosenNumber > 99)
    {
      Alert.alert('Invalid number!', 'Number has to be a number betweeen 1 and 99.',
      [{text:"okay", style:"destructure", onPress: resetInputHandler}])
      return;
    } 
    // console.log(choosenNumber)
    onPickNumber(choosenNumber)

  }
  const marginTopDistance = height < 300 ? 30: 100; 
    return (
      <ScrollView style={styles.screen}>
      <KeyboardAvoidingView style={styles.screen} behavior="position">
      <View style={[styles.rootContainer,{marginTop:marginTopDistance}]}>
        <Title>Guess My Number</Title>
      <Card style={styles.inputContainer}>
        <InstructionText>Enter a Number</InstructionText>
          <TextInput
           style={styles.numberInput}
           keyboardType='number-pad'
           maxLength={2}
           autoCapitalize='none'
           autoCorrect={false}
           value={enteredNumber}
           onChangeText={numberInputHandler}
           />
           <View style={styles.buttonsContainer}>
             <View style={styles.buttonContainer}>
                <PrimaryButton onPress={resetInputHandler}>Reset</PrimaryButton>
             </View>
             <View style={styles.buttonContainer}>
             <PrimaryButton onPress={confirmInputHandler}>Confirm</PrimaryButton>
             </View>
           </View>
      </Card>
      </View>
      </KeyboardAvoidingView>
       </ScrollView>
    )
  }
  export default StartGameScreen
  const deviceHeight =  Dimensions.get('window').height;
  const styles = StyleSheet.create({
    screen:{
      flex:1,
    },
    rootContainer:{
      flex:1,
      marginTop: deviceHeight < 380 ? 30 :100,
      alignItems:'center'

    },
    instructionText:{
      color:Colors.accet500,
      fontSize:24,
    }
    ,
    inputContainer: {
      justifyContent: 'center',
      alignItems:'center',
      marginTop: 36,
      marginHorizontal: 24,
      padding: 16,
      backgroundColor: Colors.primary500,
      borderRadius: 8,
      elevation: 4,
      shadowColor: "black",
      shadowOffset: {width: 0, height: 2},
      shadowRadius: 6,
      shadowOpacity: 0.25
    },
    numberInput: {
      height: 50,
      width: 50,
      fontSize: 32,
      borderBottomColor:Colors.accet500,
      borderBottomWidth: 2,
      color: Colors.accet500,
      marginVertical: 8,
      fontWeight: 'bold',
      textAlign: 'center'
  },
  buttonsContainer: {
      flexDirection: 'row'
  },
  buttonContainer: {
      flex: 1,
  }
  })
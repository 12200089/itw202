import { View, Text,StyleSheet } from 'react-native'
import React from 'react';
import Colors from "../../constants/colors"

const GuessLogItem = ({roundNumber, guess}) => {
  return (
    <View style={styles.ListItem} >
        <Text style={styles.itemText}>#{roundNumber}</Text>
      <Text style={styles.itemText}>Opponent's Guess:{guess}</Text>
    </View>
  )
}

export default GuessLogItem
const styles = StyleSheet.create({
    ListItem:{
        borderColor: Colors.primary800,
        borderWidth: 1,
        borderWidth:1,
        borderRadius:40,
        padding:12,
        marginVertical:8,
        backgroundColor:Colors.accet500,
        flexDirection:'row',
        justifyContent:'space-between',
        width:"100%",
        elevation:4,
        shadowColor:"black",
        shadowOpacity:0.25,
        shadowOffset:{width:0, height:0},
        shadowRadius:3,
    },
    itemText:{
      fontFamily: 'open-sans',
    },

})
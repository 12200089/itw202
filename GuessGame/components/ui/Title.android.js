import { Platform, StyleSheet, Text, View } from 'react-native'
import React from 'react'


export default function Title({children}) {
  return (
    <Text style={styles.title}>{children}</Text>
  )
}

const styles = StyleSheet.create({
    title:{
        // borderWidth: Platform.select({ios: 0, android: 2}),
        borderColor: "white",
        borderRadius:2,
        textAlign:"center",
        color:'white',
        fontSize:24,
        padding:12,
        fontFamily:"open-sans-bold",
        maxWidth:"80%",
        width:300,
     
      },
})
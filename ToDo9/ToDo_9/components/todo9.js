import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';

const MyF = () => {
    return (
        <View style={sty.container}>
            <Image style={sty.img1} source={{uri: 'https://picsum.photos/100/100'}}/>
            <Image style={sty.img2} source={require('../assets/react-native.png')}/>

        </View>
    )
}
export {MyF};
const sty = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    img1: {
        height: 100,
        width: 100
    },
    img2: {
        height: 100,
        width: 100
    }
})
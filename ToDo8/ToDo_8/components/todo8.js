import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const MyF = () => {
    return (
        <View style={sty.container}>
            <View style={sty.v2}>
                <Text style={sty.t1}>The <Text style={sty.t2}>quick brown fox</Text> jumps over the lazy dog</Text>
            </View>
        </View>
    )
}
export {MyF};
const sty = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 150
    },
    t1: {
        fontSize: 21   
    },
    t2: {
        fontWeight: 'bold'
    }
})